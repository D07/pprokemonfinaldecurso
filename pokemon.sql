DROP DATABASE `Pokemon`;
CREATE DATABASE `Pokemon`;
USE `Pokemon`;
DROP TABLE IF EXISTS `entrenadores`;
CREATE TABLE `entrenadores` (
  `id_nombre` INT(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `genero` varchar(1) NOT NULL,
  `pokecuartos` integer unsigned NOT NULL,
  `pokeball` INT(10) NOT NULL,
  `pocimas` INT(10) unsigned NULL,
  `fecha_nacimiento` DATE NOT NULL,
  PRIMARY KEY (`id_nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

LOCK TABLES `entrenadores` WRITE;

INSERT INTO `entrenadores` VALUES (1,'klever', 'M', 200, 10, 0,'1993-05-26');

UNLOCK TABLES;



DROP TABLE IF EXISTS `pokemons`;

CREATE TABLE `pokemons` (
  `id_pokemon` INT(10) NOT NULL AUTO_INCREMENT,
  `entrenador` INT(10) NOT NULL,
  `nivel` INT(10) NOT NULL,
  `especie` varchar(20) NOT NULL,
  `mote` varchar(20) DEFAULT NULL,
  `ps` INT(10) NOT NULL,
  `ataque` INT(10) NOT NULL,
  `defensa` INT(10) NOT NULL,
  `ataqueEspecial` INT(10) NOT NULL,
  `defensaEspecial` INT(10) NOT NULL,
  `velocidad` integer unsigned NOT NULL,
  PRIMARY KEY (`id_pokemon`),
  KEY `entrenador` (`entrenador`),
  CONSTRAINT `pokemons_ibfk_1` FOREIGN KEY (`entrenador`) REFERENCES `entrenadores` (`id_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pokemons` WRITE;

UNLOCK TABLES;

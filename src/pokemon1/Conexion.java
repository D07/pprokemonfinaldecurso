
package pokemon1;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author klever
 */

public class Conexion {
    protected Connection conexion;
    
    protected String url = "jdbc:mysql://localhost:3306/pokemon?useServerPrepStmts=true";
    protected String user = "klever21";
    protected String pass ="";
    
    /**
     * Constructor conexion util para enlazarnos a la base de datos de Pokemon.
     * Tenemos que importar el java.sql.*
     * @throws ClassNotFoundException 
     */
    public Conexion() throws ClassNotFoundException {
       try{
           //Class.forName("JDBC");
           conexion = DriverManager.getConnection(this.url, this.user,this.pass);
        }
        catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    /**
     * metodo que nos serirvira para establecer la conexion con la base de datos
     * Tenemos que llamarlo desde otra clase.
     * @return 
     */
    public Connection getConexion(){
        return this.conexion;
    }/**
     * metodo para cerrar la conexion con la base de datos 
     * @return 
     */
    public Connection cerrarConexion(){
        try {
            conexion.close();      
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conexion;
    }
    
    
        
}

   

   
    
  
    


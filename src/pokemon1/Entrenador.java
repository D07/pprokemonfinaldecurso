package pokemon1;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static pokemon1.Pokemon1.ListarPokemon;
//import static pokemon1.Pokemon1.Tienda;
//import static pokemon1.Pokemon1.CrearCharmander;
//import static pokemon1.Pokemon1.CrearPikachu;
//import static pokemon1.Pokemon1.CrearSquirtle;
//import static pokemon1.Pokemon1.GuardarPokemon;

/**
 *
 * @author klever
 */
public class Entrenador {

    protected String nombre;
    protected int pokeball = 10;
    protected int pokecuartos = 0;
    protected int pocimas = 0;

    /**
     * Creamos los constructor de Entrenador que despues en el main podremos
     * invocar todas las acciones que este puede realizar
     *
     * @param entrenador
     *
     */
    public Entrenador(String nombre) {
        this.nombre = nombre;
        this.pokeball = 10;
    }

    /**
     * Dispone de setters para establecer valores a los atributos de pokemon
     *
     * @return de los diferentes getters que no seran utiles para llamar a un
     * valor dado que nos hara falta durante la ejecucion del codigo
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPokeball() {
        return pokeball;
    }

    public void setPokeball(int pokeball) {
        this.pokeball = pokeball;
    }

    public int getPokecuartos() {
        return pokecuartos;
    }

    public void setPokecuartos(int pokecuartos) {
        this.pokecuartos = pokecuartos;
    }

    public int getPocimas() {
        return pocimas;
    }

    public void setPocimas(int pocimas) {
        this.pocimas = pocimas;
    }

    /**
     * @param temp ya que no hace falta para metodos del propio entrenador
     * @throws java.lang.ClassNotFoundException El metodo de CapturarPokemon se
     * verifica el numero de pokeballs que tiene el entrenador si no las tuviera
     * no se capotura ningun pokemon. Ademas en el metodo tenemos la
     * probabilidad de captura de un pokemon y su nivel, cuyo valor dependera el
     * si se capotura un pokemon o el nivel que tendrá.
     */
    public void CapturarPokemon(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        pokeball--;
        if (pokeball > 0) {
            Random r = new Random();
            int nivel = r.nextInt(100) + 1;
            int probabilidad = r.nextInt(99);
            if (probabilidad <= 33) {
                pokecuartos += 100;
                System.out.println("*****************************");
                System.out.println("1. puedes capturar un pokemon:"
                        + " "
                        + "\n2. huir"
                        + "");
                int respuesta = in.nextInt();
                int pokemon = r.nextInt(3) + 1;
                switch (respuesta) {
                    case 1:
                        switch (pokemon) {
                            case 1:
                                CrearCharmander(nivel, temp);
                                
                                Menu(temp);
                                break;
                            case 2:
                                CrearPikachu(nivel, temp);
                                Menu(temp);
                                break;
                            case 3:
                                CrearSquirtle(nivel, temp);
                                Menu(temp);
                                break;
                        }
                        System.out.println(pokeball);
                        Menu(temp);
                    case 2:
                        Menu(temp);
                }
            }
        } else {
            System.out.println("No te quedan pokeballs.");
            Menu(temp);
        }
    }

    /**
     * Creamos un pokemon como charmander. Que dependiendo si nos introduce el
     * usuario un nombre creamos un pokemon con el o sin el, en cuyo caso el
     * nombre sera el nombre de especie.
     *
     * @param nivel
     * @param temp en el entrenador que tenemos que llevarlo para despues poder
     * realizar una serie de acciones necesarias.
     * @throws ClassNotFoundException
     */
    public static void CrearCharmander(int nivel, Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("**CHARMANDER**");
        System.out.println("lvl: " + nivel);
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String especie = "Charmander";
        if (mote.equals("")) {
            Pokemon p1 = new Pokemon(nivel, especie);
            GuardarPokemon(p1, temp);

        } else {
            Pokemon p1 = new Pokemon(nivel, especie, mote);
            GuardarPokemon(p1, temp);

        }
    }

    /**
     * Creamos un pokemon como pikachu. Que dependiendo si nos introduce el
     * usuario un nombre creamos un pokemon con el o sin el, en cuyo caso el
     * nombre sera el nombre de especie.
     *
     * @param nivel
     * @param temp en el entrenador que tenemos que llevarlo para despues poder
     * realizar una serie de acciones necesarias.
     * @throws ClassNotFoundException
     */
    public static void CrearPikachu(int nivel, Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("**PIKACHU**");
        System.out.println("lvl: " + nivel);
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String especie = "Pikachu";
        if (mote.equals("")) {
            Pokemon p1 = new Pokemon(nivel, especie);
            GuardarPokemon(p1, temp);
        } else {
            Pokemon p1 = new Pokemon(nivel, especie, mote);
            GuardarPokemon(p1, temp);
        }
    }

    /**
     * Creamos un pokemon como squirtle. Que dependiendo si nos introduce el
     * usuario un nombre creamos un pokemon con el o sin el, en cuyo caso el
     * nombre sera el nombre de especie.
     *
     * @param nivel
     * @param temp en el entrenador que tenemos que llevarlo para despues poder
     * realizar una serie de acciones necesarias.
     * @throws ClassNotFoundException
     */
    public static void CrearSquirtle(int nivel, Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("**Squirtle**");
        System.out.println("lvl: " + nivel);
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String especie = "Squirtle";
        if (mote.equals("")) {
            Pokemon p1 = new Pokemon(nivel, especie);
            GuardarPokemon(p1, temp);

        } else {
            Pokemon p1 = new Pokemon(nivel, especie, mote);
            GuardarPokemon(p1, temp);
        }
    }

    private static PreparedStatement conexion(String insert_into_entrenadoresnombre_pokeballpo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Este metodo de GuardarPokemon, nos guarad el tipo de pokemon en una base
     * de datos. Aqui llamamos a un clase creada previamente llamada conexion
     * que no hara de enlace entre la aplicacion y la base de datos.
     *
     * @param p1 pasamos el pokemon creado como parametro para poder guardarlo.
     * @param temp y el entrenador para hacer la diferentes setencias, asi
     * relacionamos la tabla de pokemons con la de entrenadores.
     * @throws ClassNotFoundException
     */
    public static void GuardarPokemon(Pokemon p1, Entrenador temp) throws ClassNotFoundException {
        try {

            Conexion conexion = new Conexion();
            ResultSet result;
            int valor = 0;
            try (Connection con = conexion.getConexion()) {
                System.out.println("conectado");
                //buscamos el id del entrenador para poder hacer la query
                //asi enlzamos el pokemons con el entrenador.
                String sentencia1 = "SELECT id_nombre from Entrenadores where nombre like ?";
                PreparedStatement consulta = con.prepareStatement(sentencia1);
                consulta.setString(1, temp.getNombre());
                result = consulta.executeQuery();
                if (result.next()) {
                    valor = result.getInt("id_nombre");
                }
                //System.out.println(valor);

                String sentencia = "INSERT INTO pokemons(entrenador, especie, mote,ps, ataque, ataque_es, defensa, defensa_es, velocidad) VALUES (?,?,?,?,?,?,?,?,?)";
                PreparedStatement query = con.prepareStatement(sentencia);
                query.setInt(1, valor);
                query.setString(2, p1.getEspecie());
                query.setString(3, p1.getMote());
                query.setInt(4, p1.getPs());
                query.setInt(5, p1.getAtaque());
                query.setInt(6, p1.getAtaque_e());
                query.setInt(7, p1.getDefensa());
                query.setInt(8, p1.getDefensa_e());
                query.setInt(9, p1.getVelocidad());
                //System.out.println("query creada");
                //query.execute();
                query.executeUpdate();
                System.out.println(query.executeUpdate());
                result.close();
                con.close();
            }

            //System.out.println("query hecha");
        } catch (SQLException | NullPointerException ex) {
            System.out.println(" Error en query: " + ex);
        }

    }

    public void ListarPokemon(Entrenador temp) {
        try {
            int valor = 0;
            ResultSet result = null;
            Conexion conexion = new Conexion();
            Connection con = conexion.getConexion();
            String sentencia = "select id_nombre from estrenadores where nombre like ?";
            PreparedStatement query = con.prepareStatement(sentencia);
            query.setString(1, temp.getNombre());
            if (result.next()) {
                valor = result.getInt("id_nombre");
            }
            String sentencia2 = "Select especie, mote,atque, defensa, velocidad from pokemons where id_nombre = ?";
            PreparedStatement query2 = con.prepareStatement(sentencia2);
            query2.setInt(1, valor);
            int cont = 0;
            while (result.next()) {
                cont ++;
                System.out.println(cont);
                System.out.println("=========================================");
                System.out.println("pokemons : " + result.getString("especie"));
                System.out.println("mote : " + result.getString("mote"));
                System.out.println("ps : " + result.getInt("ps"));
                System.out.println("ataque : " + result.getInt("ataque"));
                System.out.println("defensa : " + result.getInt("defensa"));
                System.out.println("velocidad : " + result.getInt("velocidad"));
            }
            }catch (ClassNotFoundException ex) {
            Logger.getLogger(Entrenador.class.getName()).log(Level.SEVERE, null, ex);
        }catch (SQLException ex) {
            Logger.getLogger(Entrenador.class.getName()).log(Level.SEVERE, null, ex);
        }

        }
        /**
         * metodo que no sera util para comprar las pokeball y pocima
         *
         * @param temp
         * @throws ClassNotFoundException
         */
    public void Tienda(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("==============");
        System.out.println("**Productos**"
                + "==============="
                + " "
                + "\n 1.pokeball(100$/unidad)"
                + "\n 2.pocimas (200$/unidad)"
                + "\n 3.salir "
                + "\n Dispones de: " + pokecuartos);
        System.out.print("Elige opcion: ");
        if (pokecuartos > 100) {
            int opcion = in.nextInt();
            switch (opcion) {
                case 1:
                    ComprarPokeball(temp);
                    break;
                case 2:
                    ComprarPocimas(temp);
                    break;
                case 3:
                    Menu(temp);
                default:
                    break;
            }
        } else {
            System.out.println("NO dispones de Pokecuartos suficientes.");
            Menu(temp);
        }

    }

    /**
     * menu.Lo primero que no sale cuando cargamos la partida o inciamos una
     * nueva.
     *
     * @param temp
     * @throws ClassNotFoundException
     */
    public void Menu(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("===================");
        System.out.println("1. Capturar Pokemon"
                + "\n2. Listar Pokemon"
                + "\n3. Tienda"
                + "\n4. salir");
        int eleccion = in.nextInt();
        switch (eleccion) {
            case 1:
                CapturarPokemon(temp);//CapturarPokemon();
                break;
            case 2:
                ListarPokemon(temp);
                break;
            case 3:
                Tienda(temp);
                break;
            default:
                break;
        }
    }

    public void ComprarPokeball(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("******************************");
        System.out.println("¿Digite el numero de pokeball?");
        System.out.println(" ");
        int num = in.nextInt();
        int pokesGastados = num * 100;
        if (pokecuartos == pokesGastados) {
            pokeball = pokeball + num;
            pokecuartos = 0;
            System.out.println("Te quedan " + pokecuartos + " $");
            if (pokecuartos < pokesGastados) {
                System.out.println(" ");
                System.out.println("no puedes comprar ese numero de pokeballs");
                ComprarPokeball(temp);
            }
            if (pokecuartos > pokesGastados) {
                pokeball = pokeball + num;
                pokecuartos = pokecuartos - pokesGastados;
                System.out.println("te quedan: " + pokecuartos);
            }
        }
        Tienda(temp);

    }

    public void ComprarPocimas(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("******************************");
        System.out.println("¿Digite el numero de pocimas?");
        System.out.println(" ");
        int num = in.nextInt();
        int pokesGastados = num * 200;
        if (pokecuartos == pokesGastados) {
            pocimas = pocimas + num;
            pokecuartos = 0;
            System.out.println("Te quedan " + pokecuartos + " $");
        }
        if (pokecuartos < pokesGastados) {
            System.out.println(" ");
            System.out.println("no puedes comprar ese numero de pokeballs");
            ComprarPokeball(temp);
        }
        if (pokecuartos > pokesGastados) {
            pocimas = pocimas + num;
            pokecuartos = pokecuartos - pokesGastados;
            System.out.println("te quedan: " + pokecuartos);
        }
        Tienda(temp);
    }
}

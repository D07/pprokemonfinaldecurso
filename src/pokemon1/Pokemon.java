
package pokemon1;

/**
 *
 * @author klever
 */
public class Pokemon {
    protected String especie ;
    protected String mote;
    protected int nivel;
    protected int ps = 2; 
    protected int ataque =3;
    protected int ataque_e=5;
    protected int defensa=2;
    protected int defensa_e=5;
    protected int velocidad=3;
    
    //construcutor de pokemons

    public Pokemon(int nivel, String especie) {
        this.nivel = nivel;
        this.especie = especie;
        this.mote = especie;
        
    }

    public Pokemon(int nivel, String especie,String mote) {
        this.nivel = nivel;
        this.especie = especie;
        this.mote = mote;
    }
    
    
    //getters and setters
    public int getNivel() {    
        return nivel;
    }
    public void setNivel(int nivel) {    
        this.nivel = nivel;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getMote() {
        return mote;
    }

    public void setMote(String mote) {
        this.mote = mote;
    }

    public int getPs() {
        return (ps*nivel);
    }

    public void setPs(int ps) {
        this.ps = ps;
    }

    public int getAtaque() {
        return (ataque*nivel);
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getAtaque_e() {
        return (ataque_e*nivel);
    }

    public void setAtaque_e(int ataque_e) {
        this.ataque_e = ataque_e;
    }

    public int getDefensa() {
        return (defensa*nivel);
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getDefensa_e() {
        return (defensa_e*nivel);
    }

    public void setDefensa_e(int defensa_e) {
        this.defensa_e = defensa_e;
    }

    public int getVelocidad() {
        return (velocidad*nivel);
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }
    
}

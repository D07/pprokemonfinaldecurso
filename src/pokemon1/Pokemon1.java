package pokemon1;

import java.sql.*;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author klever
 */
public class Pokemon1 {

    public static void main(String[] args) throws SQLException {

        
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("=================");
            System.out.println("1. Cargar Partida"
                    + "\n2. Nueva Partida"
                    + "\n3. Salir");
            int eleccion = in.nextInt();
            switch (eleccion) {
                case 1:
                    CargarPartida();
                    break;
                case 2:
                    NuevaPartida();
                    break;
                default:
                    break;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Pokemon1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void CargarPartida() throws ClassNotFoundException, SQLException {
        Scanner in = new Scanner (System.in);
        System.out.println("*********************");
        System.out.println("introduce un nombre: ");
        System.out.println("*********************");
        System.out.println(" ");
        String nombre = in.nextLine();
        Entrenador temp = new Entrenador(nombre);
        ResultSet result;
        Conexion conexion = new Conexion();
        int valor = 0;
        Connection con = conexion.getConexion();
        String sentencia ="select id_nombre from entrenadores where nombre like ? ";
        //preparamos la query para que sea enviada
        PreparedStatement query = con.prepareStatement(sentencia);
        query.setString(1, nombre);
        result = query.executeQuery();
        if (result.next()){
            valor = result.getInt("id_nombre");
        }
       
        System.out.println(valor);
        
        //con el nombre queremos buscar todos los pokemons que tiene el entrenador.
        
        String sentencia1 ="select especie, mote, ps, ataque, defensa, velocidad from pokemons where entrenador = ?";
        PreparedStatement query2 = con.prepareStatement(sentencia1);
        query2.setInt(1, valor);
        System.out.println("query creada");
        result = query2.executeQuery();
         System.out.println("**********************************");
         System.out.println("Estos son los pokemons que tienes:");
         System.out.println("**********************************");
        while(result.next()){
            System.out.println("=========================================");
            System.out.println("pokemons : "+ result.getString("especie"));
            System.out.println("mote : "+ result.getString("mote"));
            System.out.println("ps : "+ result.getInt("ps"));
            System.out.println("ataque : "+ result.getInt("ataque"));
            System.out.println("defensa : "+ result.getInt("defensa"));
            System.out.println("velocidad : "+ result.getInt("velocidad"));
     
        }
        result.close();
        query.close();
        //result2.close();
        query2.close();
        con.close();
        temp.Menu(temp);
        
    }

    public static void NuevaPartida() throws ClassNotFoundException {
            Scanner in = new Scanner(System.in);
            System.out.println("****************");
            System.out.println("introduce nombre: ");
            System.out.println("****************");
            String nombre = in.nextLine();
            if(nombre.equals("")){
                System.out.println("ERROR. No has introducido nombre."
                        + "\n Debes introducido nombre: ");
                        in.nextLine();
            }else{
            Entrenador temp = new Entrenador(nombre);
                System.out.println("===================================");
                System.out.println("**Bienvenido al juego de Pokemon**");
                System.out.println("===================================");
                System.out.println(" ");
                System.out.println("Tienes un total de "+temp.getPokeball()+" pakeball");
                System.out.println("Tienes un total de "+temp.getPocimas()+" pocimas");
            GuardarDatos(temp);
            temp.Menu(temp);
            }
    }
 
    /*public void menu(Entrenador temp) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("1. Capturar Pokemon"
                + "\n2. Listar Pokemon"
                + "\n3. Tienda"
                + "\n4. salir");
        int eleccion = in.nextInt();
        switch (eleccion) {
            case 1:
                 temp.CapturarPokemon();//CapturarPokemon();
                break;
            case 2:
                ListarPokemon(temp);
                break;
            case 3:
                Tienda(temp);
                break;
            default:
                break;
        }
    }*/

    /*public static void CapturarPokemon() throws ClassNotFoundException {
        Random r = new Random();
        int nivel = r.nextInt(100) + 1;
        int probabilidad = r.nextInt(100) + 1;
        if (probabilidad <= 66) {
            int pokemon = r.nextInt(3);
            switch (pokemon) {
                case 1:
                    CrearCharmander(nivel);
                    break;
                case 2:
                    CrearPikachu(nivel);
                    break;
                case 3:
                    CrearSquirtle(nivel);
                    break;
            }
        }
    }*/

    public static void ListarPokemon(Entrenador temp) {

    }

    public static void Tienda(Entrenador temp) {

    }

   /* public static void CrearCharmander(int nivel) throws ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        System.out.println("**CHARMANDER**");
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String nombre="Charmander";
        if(mote.equals("")){
            Pokemon temp = new Pokemon(nivel,nombre);
            GuardarPokemon(temp);
            
        }else{
            Pokemon temp = new Pokemon(nivel,nombre,nombre);
            GuardarPokemon(temp);
            
        }     
    }
    public static void CrearPikachu(int nivel) throws ClassNotFoundException{
        Scanner in = new Scanner(System.in);
        System.out.println("**PIKACHU**");
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String nombre="Pikachu";
        if(mote.equals("")){
            Pokemon temp = new Pokemon(nivel,nombre);
            GuardarPokemon(temp);
        }else{
            Pokemon temp = new Pokemon(nivel,nombre,nombre);
            GuardarPokemon(temp);
        }
    }
    public static void CrearSquirtle(int nivel) throws ClassNotFoundException{
        Scanner in = new Scanner(System.in);
        System.out.println("**Squirtle**");
        System.out.print("Intrduce un mote: ");
        String mote = in.nextLine();
        String nombre="Squirtle";
        if(mote.equals("")){
            Pokemon temp = new Pokemon(nivel, nombre);
            GuardarPokemon(temp);
        }else{
            Pokemon temp = new Pokemon(nivel, nombre,nombre);
            GuardarPokemon(temp);
        }
    }

    private static PreparedStatement conexion(String insert_into_entrenadoresnombre_pokeballpo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    private static void GuardarDatos( Entrenador temp) throws ClassNotFoundException{
        
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.getConexion();
            System.out.println("conectado");
            PreparedStatement query;
            String sentencia = "INSERT INTO entrenadores(nombre, pokeball,pokecuartos,pocimas) VALUES (?,?,?,?)";
            query = con.prepareStatement(sentencia);
            System.out.println("query creada");
            query.setString(1,temp.getNombre());
            query.setInt(2, temp.getPokeball());
            query.setInt(3, temp.getPokecuartos());
            query.setInt(4, temp.getPocimas());
            query.execute();
            con.close();
            System.out.println("query hecha");
        } catch (SQLException ex) {
            System.out.println(ex);
        } catch (NullPointerException ex){
            System.out.println(ex);  
        }
    }
   /* public static void GuardarPokemon(Pokemon temp) throws ClassNotFoundException{
        try{
            Conexion conexion = new Conexion();
            Connection con = conexion.getConexion();
            System.out.println("conectado");
            PreparedStatement query;
            String sentencia = "INSERT INTO entrenadores(entrenador, especie, mote,ps, ataque, ataque_es, defensa, defensa_es, velocidad) VALUES (?,?,?,?,?,?,?,?)";
            query = con.prepareStatement(sentencia);
            System.out.println("query creada");
            query.setString(1,temp.getNombre());
            query.setString(2, temp.getNombre());
            query.setString(3, temp.getMote());
            query.setInt(4, temp.getPs());
            query.setInt(5, temp.getAtaque());
            query.setInt(6, temp.getAtaque_e());
            query.setInt(7, temp.getDefensa());
            query.setInt(8, temp.getDefensa_e());
            query.setInt(9, temp.getVelocidad());
            query.execute();
            con.close();
            System.out.println("query hecha");
        } catch (SQLException ex) {
            System.out.println(ex);
        } catch (NullPointerException ex){
            System.out.println(ex);  
        }
        
    }*/
}
